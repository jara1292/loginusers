﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class UserViewModel
    {
        public int IdUser { get; set; }

        [Display(Name = "Correo electrónico")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Debes ingresar un email valido")]
        [Required]
        public string Email { get; set; }

        [Display(Name = "Usuario")]
        [MinLength(7, ErrorMessage = "La longitud minima del campo {0} debe de ser de {1} caracteres")]
        [Required]
        public string UserName { get; set; }

        [Display(Name = "Contraseña")]
        [MinLength(10, ErrorMessage = "La contraseña debe contener al menos {0} caracteres")]
        [DataType(DataType.Password)]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{10,}$", ErrorMessage = "error en contraseña debes ingresar al menos una mayuscula, una minuscula, un número y un caracter especial")]
        [Required]
        public string Password { get; set; }


        [NotMapped]
        [DataType(DataType.Password)]
        [Display(Name = "confirmar contraseña")]
        [CompareAttribute("Password", ErrorMessage = "Contraseñas no coinciden")]
        public string VerifiedPassword { get; set; }


        [Display(Name = "Sexo")]
        [Required]
        public string Sex { get; set; }
    }
}
