﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class UserDbContext : DbContext
    {
        public UserDbContext() : base("name=Conexion")
        {

        }

        public DbSet<User> Users { get; set; }
    }
}
