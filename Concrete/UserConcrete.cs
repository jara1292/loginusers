﻿using Concrete.Helpers;
using Entities;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concrete
{
    public class UserConcrete : IUser
    {
        /// <summary>Crear usuario
        /// </summary>
        /// 
        public Boolean CreateUser(UserViewModel user)
        {
            using (var db = new UserDbContext())
            {
                var validar = ValidateUser(user);
                if (!validar)
                {
                    var usuario = ViewModelToModel(user);
                    //user.Status = true;
                    //user.Password = EncriptarPassword.EncryptPassword(user.Password);
                    //user.VerifiedPassword = EncriptarPassword.EncryptPassword(user.VerifiedPassword);
                    db.Users.Add(usuario);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        /// <summary>Eliminar usuario
        /// </summary>
        /// 
        public void DeleteUser(int user)
        {
            using (var db = new UserDbContext())
            {
                //db.Users.Remove(user);
                var usuario = db.Users.Find(user);
                usuario.Status = false;
                //usuario.VerifiedPassword = usuario.Password;
                db.SaveChanges();
            }
        }


        /// <summary>Editar usuario
        /// </summary>
        /// 
        public Boolean EditUser(UserViewModel user)
        {
            using (var db = new UserDbContext())
            {
                var validar = ValidateUser(user);
                if (!validar)
                {
                    var usuario = ViewModelToModel(user);
                    db.Entry(usuario).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        /// <summary>Obtener usuario por ID
        /// </summary>
        public UserViewModel GetUser(int id)
        {
            using (var db = new UserDbContext())
            {
                var user = db.Users.Find(id);

                UserViewModel usuario = new UserViewModel
                {
                    IdUser = user.IdUser,
                    Email = user.Email,
                    Password = user.Password,
                    Sex = user.Sex,
                    UserName = user.UserName
                };

                return usuario;
            }
        }


        /// <summary>Obtener todos los usuarios
        /// </summary>
        public IEnumerable<User> GetUsers()
        {
            using (var db = new UserDbContext())
            {
                return db.Users.ToList();
            }
        }

        


        /// <summary>Validar si usuario ya existe
        /// </summary>
        /// 
        public bool ValidateUser(UserViewModel user)
        {
            using (var db = new UserDbContext())
            {
                var query = db.Users.Where(x => x.Email == user.Email);
                return (query.Any() && user.IdUser != query.FirstOrDefault().IdUser) ? true : false;

            }
        }

        public User ViewModelToModel(UserViewModel user)
        {
            User usuario = new User{
                IdUser = user.IdUser,
                Email = user.Email,
                Status = true,
                Password = user.Password,
                Sex = user.Sex,
                UserName = user.UserName
            };

            return usuario;
        }



        public bool LoginUser(string email, string password)
        {
            using(var db = new UserDbContext())
            {
                var query = db.Users.Where(x => x.Email == email && x.Password == password);

                return query.Any() ? true : false;
            }
        }
    }
}
