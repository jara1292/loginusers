﻿new Vue({
    el: '#contenedor',
    mounted() {
        console.log('instancia montada');
        //this.cargarPersonas();
    },
    data: {
        id: 0,
    },
    methods: {
        eliminarUsuario(idUsuario) {
            //alert(idUsuario);
            if (confirm('Esta seguro de eliminar este usuario?')) {
                axios.post('Delete/', {
                    user: idUsuario,
                })
                    .then((response) => {
                        alert('usuario eliminado');
                        //alert(response);
                        console.log(response);
                        window.location.reload();
                    })
                    .catch((error) => {
                        alert('error al eliminar usuario' + error);
                        console.log(error);
                    });
            }
        }
    }
})