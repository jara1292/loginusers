﻿using Concrete.Helpers;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenUsuarios.Controllers
{
    public class LoginController : Controller
    {

        private readonly IUser _IUser;

        public LoginController(IUser Iuser)
        {
            _IUser = Iuser;
        }


        // Login
        public ActionResult Inicio()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Inicio(string password, string email)
        {
            password = EncriptarPassword.EncryptPassword(password);
            var validar = _IUser.LoginUser(email, password);
            if (validar)
            {
                Session["usuario"] = email;
                return RedirectToAction("Index","User",null);
            }
            else
            {
                ViewBag.Mensaje = "usuario y/o contraseña incorrecto(s)";
                return View();
            }
        }


        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Inicio");
        }
    }
}