﻿using Concrete.Helpers;
using Entities;
using ExamenUsuarios.Helpers;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenUsuarios.Controllers
{
    public class UserController : Controller
    {
        private readonly IUser _Iuser;

        public UserController(IUser Iuser)
        {
            _Iuser = Iuser;
        }



        public ActionResult Index()
        {
            try
            {
                return View(_Iuser.GetUsers());
            }
            catch (Exception)
            {

                throw;
            }
        }

        [FiltroAutoriza]
        public ActionResult Create()
        {
            var list = new SelectList(new[]
                                         {
                                              new {ID="",Name="--SELECCIONE SEXO"},
                                              new {ID="MASCULINO",Name="MASCULINO"},
                                              new{ID="FEMENINO",Name="FEMENINO"},
                                          },
              "ID", "Name", 1);
            ViewData["sexo"] = list;

            return View();
        }

        [HttpPost]
        public ActionResult Create(UserViewModel user)
        {
            var list = new SelectList(new[]
                                         {
                                              new {ID="",Name="--SELECCIONE SEXO"},
                                              new {ID="MASCULINO",Name="MASCULINO"},
                                              new{ID="FEMENINO",Name="FEMENINO"},
                                          },
              "ID", "Name", 1);
            ViewData["sexo"] = list;

            try
            {

                user.Password = EncriptarPassword.EncryptPassword(user.Password);
                user.VerifiedPassword = EncriptarPassword.EncryptPassword(user.VerifiedPassword);

                var crear = _Iuser.CreateUser(user);
                if (crear)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Mensaje = "Ya existe un usuario con el email: " + user.Email;
                    return View(user);
                }
                
            }
            catch (DbEntityValidationException ex)
            {
                var error = "";
                TempData["Error"] = "";

                foreach (var eve in ex.EntityValidationErrors)
                {
                    error += eve.Entry.Entity.GetType();
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
                TempData["Error"] = error;
                return View();

            }
        }


        [FiltroAutoriza]
        public ActionResult Edit(int id)
        {
            var list = new SelectList(new[]
                                         {
                                              new {ID="",Name="--SELECCIONE SEXO"},
                                              new {ID="MASCULINO",Name="MASCULINO"},
                                              new{ID="FEMENINO",Name="FEMENINO"},
                                          },
              "ID", "Name", 1);
            ViewData["sexo"] = list;
            return View(_Iuser.GetUser(id));
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel user)
        {
            var list = new SelectList(new[]
                                         {
                                              new {ID="",Name="--SELECCIONE SEXO"},
                                              new {ID="MASCULINO",Name="MASCULINO"},
                                              new{ID="FEMENINO",Name="FEMENINO"},
                                          },
              "ID", "Name", 1);
            ViewData["sexo"] = list;
            try
            {
                user.Password = EncriptarPassword.EncryptPassword(user.Password);
                user.VerifiedPassword = EncriptarPassword.EncryptPassword(user.VerifiedPassword);
                var actualizar = _Iuser.EditUser(user);

                if (actualizar)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Mensaje = "El email: " + user.Email + " ya ha sido dado de alta";
                    return View(user);
                }

                
            }
            catch (Exception)
            {

                throw;
            }
        }




        [FiltroAutoriza]
        [HttpPost]
        public string Delete(int user)
        {
            try
            {
                _Iuser.DeleteUser(user);
                return "OK";
                
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
                throw;
            }
        }
    }
}