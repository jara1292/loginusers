﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenUsuarios.Helpers
{
    public class FiltroAutoriza : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                return;
            }

            if (HttpContext.Current.Session["usuario"] == null)
            {
                //filterContext.Result = new HttpUnauthorizedResult();
                filterContext.Result = new RedirectResult("~/Login/Inicio");
            }
        }
    }
}