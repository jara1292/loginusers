﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IUser
    {
        /// <summary>Obtener todos los usuarios
        /// </summary>
        IEnumerable<User> GetUsers();


        /// <summary>Obtener usuario por ID
        /// </summary>
        UserViewModel GetUser(int id);

        /// <summary>Crear usuario
        /// </summary>
        /// 
        Boolean CreateUser(UserViewModel user);


        /// <summary>Editar usuario
        /// </summary>
        /// 
        Boolean EditUser(UserViewModel user);


        /// <summary>Validar si usuario ya existe
        /// </summary>
        /// 
        Boolean ValidateUser(UserViewModel user);

        /// <summary>Eliminar usuario
        /// </summary>
        /// 
        void DeleteUser(int user);


        User ViewModelToModel(UserViewModel user);

        Boolean LoginUser(string email, string password);
    }
}
